﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;

using NationalInstruments.TestStand.Interop.API;

using SerialNumberFunctionsDLL;


//
//  History
//
//  1.00
//      first release
//  1.01
//      fixed error codes

//================================================================
//
//  Serial number routines for TestStand
//
// Revs
//  4/8/2019    LN
//      GetUUTData wrote and tested.
//

namespace TSSerialNumberDLL
{
    
    public struct DatabaseInfo
    {
        public string ServerIP;
        public string User;
        public string Password;
        public string SerialNumbersUsedTable;
    }
    public class TSSerialNumberFunctions
    {
         const int systemerrorbase = -8000;       // system errors. range of -8000 to -8024. Serial number database returns negitive values
        const int usererrorbase = 5000;             // user type errors. range of 5000 to 5024. serial number database return positive values
        
        /// <summary>
        /// Check to see if the PCBA serial number is in the Serial Number Database.
        /// </summary>
        /// <remarks>
        /// Will look in the Serial Number Database to find the PCBA serial number.
        /// If found, the MAC database will be serached for the MACs that are assigned to the PCBA SN.
        /// If no PCBA serial number is found, then the returned values will be empty strings.
        /// </remarks>
        /// <param name="DatabaseInfo">structure with the database loging and table names</param>
        /// <param name="pcbasn">string - PCBA serial number to look for</param>
        /// <param name="HLAsn">string - HLA serial number if PCBA SN was found. empty string if no PCBA SN found</param>
        /// <param name="mac1">string - first MAC if PCBA SN was found. empty string if no PCBA SN found</param>
        /// <param name="mac2">string - second MAC if PCBA was found. empty string if no PCBA SN found or there is no second MAC</param>
        /// <param name="errorOccurred">bool - true = system issue found</param>
        /// <param name="errorCode">int - error code</param>
        /// <param name="errorMsg">string - error message</param>
        public bool CheckForPCBASNdata(DatabaseInfo factorydata, string pcbasn, 
                                        out string HLAsn, out string mac1, out string mac2, 
                                        out bool errorOccurred, out int errorCode, out string errorMsg)
        {
            errorOccurred = false;
            errorCode = 0;
            errorMsg = String.Empty;
            HLAsn = string.Empty;
            mac1 = string.Empty;
            mac2 = string.Empty;

            using (SerialNumberFunctions snDB = new SerialNumberFunctions(factorydata.ServerIP, factorydata.SerialNumbersUsedTable, factorydata.User, factorydata.Password, ""))
            {
                if (!snDB.CheckForDatabase())  // make sure the database will open
                {
                    errorCode = systemerrorbase + snDB.LastErrorCode;
                    errorMsg = snDB.LastErrorMessage;
                    errorOccurred = true;
                    return false;
                }

                // see if the serial number is in the serial number database
                if (!snDB.LookForPCBASN(pcbasn, ref HLAsn, ref mac1, ref mac2))
                {
                    if (snDB.LastErrorCode != 0)
                    {
                        errorCode = systemerrorbase + snDB.LastErrorCode;
                        errorMsg = snDB.LastErrorMessage;
                        errorOccurred = true;
                    }
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Check to see if the HLA serial number is in the Serial Number Database.
        /// </summary>
        /// <remarks>
        /// Will look in the Serial Number Database to find the HLA serial number.
        /// If found, the PCBA SN will be returnede.
        /// If no HLA serial number is found, then the returned values will be empty strings.
        /// </remarks>
        /// <param name="DatabaseInfo">structure with the database loging and table names</param>
        /// <param name="pcbasn">string - PCBA serial number found. Empty if HLA not found.</param>
        /// <param name="HLAsn">string - HLA serial number to look for</param>
        /// <param name="mac1">string - first MAC if PCBA SN was found. empty string if no PCBA SN found</param>
        /// <param name="mac2">string - second MAC if PCBA was found. empty string if no PCBA SN found or there is no second MAC</param>
        /// <param name="errorOccurred">bool - true = system issue found</param>
        /// <param name="errorCode">int - error code</param>
        /// <param name="errorMsg">string - error message</param>
        public bool CheckForHLASNdata(DatabaseInfo factorydata, string HLAsn,
                                        out string pcbasn, out string mac1, out string mac2,
                                        out bool errorOccurred, out int errorCode, out string errorMsg)
        {
            errorOccurred = false;
            errorCode = 0;
            errorMsg = String.Empty;
            pcbasn = string.Empty;
            mac1 = string.Empty;
            mac2 = string.Empty;

            using (SerialNumberFunctions snDB = new SerialNumberFunctions(factorydata.ServerIP, factorydata.SerialNumbersUsedTable, factorydata.User, factorydata.Password, ""))
            {
                if (!snDB.CheckForDatabase())  // make sure the database will open
                {
                    errorCode = systemerrorbase + snDB.LastErrorCode;
                    errorMsg = snDB.LastErrorMessage;
                    errorOccurred = true;
                    return false;
                }

                // see if the serial number is in the serial number database
                if (!snDB.LookForHLASN(HLAsn, ref pcbasn, ref mac1, ref mac2))
                {
                    if (snDB.LastErrorCode != 0)
                    {
                        errorCode = systemerrorbase + snDB.LastErrorCode;
                        errorMsg = snDB.LastErrorMessage;
                        errorOccurred = true;
                    }
                    return false;
                }
            }
            return true;
        }


        /// <summary>
        /// Will get or generate the needed hla serial number and macs for a given PCBA serial number.
        /// </summary>
        /// <param name="factorydata">DatabaseInfo structure</param>
        /// <param name="pcbasn">string - PCBA serial to use</param>
        /// <param name="HLAsn">string - HLA serial number</param>
        /// <param name="mac1">string - MAC 1</param>
        /// <param name="mac2">string - MAC 2 if numOfMacs = 2</param>
        /// <param name="errorOccurred">bool - true = system error</param>
        /// <param name="errorCode">int - error code of system error</param>
        /// <param name="errorMsg">string - error message</param>
        public void GetUUTData(DatabaseInfo factorydata, string pcbasn, string hlaPrefix, int numOfMacs, string MACPool,
                                        out string HLAsn, out string mac1, out string mac2,
                                        out bool errorOccurred, out int errorCode, out string errorMsg)
        {
            HLAsn = string.Empty;
            mac1 = string.Empty;
            mac2 = string.Empty;
            List<string> macList = new List<string>();
            errorOccurred = false;
            errorCode = 0;
            errorMsg = String.Empty;

            // make sure everything is upper case
            pcbasn = pcbasn.ToUpper();
            hlaPrefix = hlaPrefix.ToUpper();

            using (SerialNumberFunctions snDB = new SerialNumberFunctions(factorydata.ServerIP, factorydata.SerialNumbersUsedTable, factorydata.User, factorydata.Password, ""))
            {
                if (!snDB.CheckForDatabase())  // make sure the database will open
                {
                    errorCode = systemerrorbase + snDB.LastErrorCode;
                    errorMsg = snDB.LastErrorMessage;
                    errorOccurred = true;
                    return;
                }

                // see if the serial number is in the serial number database
                if (!snDB.LookForPCBASN(pcbasn, ref HLAsn, ref mac1, ref mac2))
                {
                    if (snDB.LastErrorCode != 0)
                    {
                        errorCode = systemerrorbase + snDB.LastErrorCode;
                        errorMsg = "LookForPCBASN: " + snDB.LastErrorMessage;
                        errorOccurred = true;
                        return;
                    }

                    // did not find the PCBA SN in database, so make new HLA SN and get macs.
                    if (!snDB.GetAndRecordNextMac(MACPool, numOfMacs, ref mac1, ref mac2))
                    {
                        errorCode = systemerrorbase + snDB.LastErrorCode;
                        errorMsg = "GetAndRecordNextMac: " + snDB.LastErrorMessage;
                        errorOccurred = true;
                        return;
                    }
                    if ((HLAsn = snDB.GetAndRecordNextHLASN(hlaPrefix)) == string.Empty)
                    {
                        errorCode = systemerrorbase + snDB.LastErrorCode;
                        errorMsg = "GetAndRecordNextHLASN: " + snDB.LastErrorMessage;
                        errorOccurred = true;
                        return;
                    }

                    // record everthing
                    macList.Clear();
                    macList.Add(mac1);
                    macList.Add(mac2);
                    if (!snDB.RecordSNData(HLAsn, pcbasn, macList))
                    {
                        errorCode = systemerrorbase + snDB.LastErrorCode;
                        errorMsg = "RecordSNData: " + snDB.LastErrorMessage;
                        errorOccurred = true;
                    }
                    return;
                }
            }


        }
    }
}
